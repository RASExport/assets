import { Injectable } from '@angular/core';
import { environment} from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class GvarService {
  serverURL : string = environment.serverURL;
  G_IsRunning: boolean = false;
  serverURLLogin : string = environment.serverURLLogin;
  constructor() { }
}
