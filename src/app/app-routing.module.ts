import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdvanceComponent } from './Pages/Advance/advance.component';
import { AddcontractComponent } from './Pages/Advance/ContractLicenses/AddContract/addcontract.component';
import { ContractlicenseComponent } from './Pages/Advance/ContractLicenses/contractlicense.component';
import { CustomerComponent } from './Pages/Advance/Customers/customer.component';
import { AddinsuranceComponent } from './Pages/Advance/Insurance/AddInsurance/addinsurance.component';
import { InsuranceComponent } from './Pages/Advance/Insurance/insurance.component';
import { PersonemployeeComponent } from './Pages/Advance/PersonEmployee/personemployee.component';
import { AddassetComponent } from './Pages/Assets/AddAssets/addasset.component';
import { AssetComponent } from './Pages/Assets/asset.component';
import { AssetlistComponent } from './Pages/Assets/AssetsList/assetlist.component';
import { CheckinComponent } from './Pages/Assets/CheckIn/checkin.component';
import { CheckoutComponent } from './Pages/Assets/CheckOut/checkout.component';
import { DisposeComponent } from './Pages/Assets/Dispose/dispose.component';
import { LeaseComponent } from './Pages/Assets/Lease/lease.component';
import { LeasereturnComponent } from './Pages/Assets/LeaseReturn/leasereturn.component';
import { MaintenanceComponent } from './Pages/Assets/Maintenance/maintenance.component';
import { MoveComponent } from './Pages/Assets/Move/move.component';
import { ReserveComponent } from './Pages/Assets/Reserve/reserve.component';
import { DashboardComponent } from './Pages/DashBoard/dashboard.component';
import { ForgrtpasswordComponent } from './Pages/ForgetPassword/forgrtpassword.component';
import { LoginComponent } from './Pages/Login/login.component';
import { RegisterComponent } from './Pages/Register/register.component';
import { ByassettagComponent } from './Pages/Reports/AssetsReport/ByAssetTag/byassettag.component';
import { BycategoryComponent } from './Pages/Reports/AssetsReport/ByCategory/bycategory.component';
import { BydepartmentComponent } from './Pages/Reports/AssetsReport/ByDepartment/bydepartment.component';
import { BylinkedassetComponent } from './Pages/Reports/AssetsReport/ByLinkedAsset/bylinkedasset.component';
import { BysitelocationComponent } from './Pages/Reports/AssetsReport/BySiteLocation/bysitelocation.component';
import { BytagwithpictureComponent } from './Pages/Reports/AssetsReport/ByTagWithPicture/bytagwithpicture.component';
import { BywarrantyinfoComponent } from './Pages/Reports/AssetsReport/ByWarrantyInfo/bywarrantyinfo.component';
import { AuditreportComponent } from './Pages/Reports/AuditReport/auditreport.component';
import { ByauditassettagComponent } from './Pages/Reports/AuditReport/ByAuditAssetTag/byauditassettag.component';
import { ByauditdateComponent } from './Pages/Reports/AuditReport/ByAuditDate/byauditdate.component';
import { ByauditsitelocationComponent } from './Pages/Reports/AuditReport/ByAuditSiteLocation/byauditsitelocation.component';
import { ByfundingComponent } from './Pages/Reports/AuditReport/ByFunding/byfunding.component';
import { LocationdiscrepancyComponent } from './Pages/Reports/AuditReport/LocationDiscrepancy/locationdiscrepancy.component';
import { NonauditassetComponent } from './Pages/Reports/AuditReport/NonAuditAsset/nonauditasset.component';
import { NonauditfundingComponent } from './Pages/Reports/AuditReport/NonAuditFunding/nonauditfunding.component';
import { AutomatedreportComponent } from './Pages/Reports/AutomatedReport/automatedreport.component';
import { CustomreportComponent } from './Pages/Reports/CustomReport/customreport.component';
import { NewreportComponent } from './Pages/Reports/CustomReport/NewReport/newreport.component';
import { ReportComponent } from './Pages/Reports/report.component';
import { LayoutComponent } from './Pages/Shared/Layout/layout.component';
import { AuditComponent } from './Pages/Tools/Audit/audit.component';
import { DocumentgalleryComponent } from './Pages/Tools/DocumentGallery/documentgallery.component';
import { ExportComponent } from './Pages/Tools/Export/export.component';
import { ImagegalleryComponent } from './Pages/Tools/ImageGallery/imagegallery.component';
import { ImportComponent } from './Pages/Tools/Import/import.component';

const routes: Routes = [
{
  path: '', component: LayoutComponent, children:[
    {path:'Dashboard',component:DashboardComponent},
   {
     path:'Asset',component:AssetComponent,children:[
       {path:'AddAsset',component:AddassetComponent},
       {path:'AssetList',component:AssetlistComponent},
       {path:'Checkout',component:CheckoutComponent},
       {path:'Checkin',component:CheckinComponent},
       {path:'Lease',component:LeaseComponent},
       {path:'LeaseReturn',component:LeasereturnComponent},
       {path:'Dispose',component:DisposeComponent},
       {path:'Maintenance',component:MaintenanceComponent},
       {path:'Move',component:MoveComponent},
       {path:'Reserve',component:ReserveComponent}
     ]
   },
   {
    path:'Tools',component:AssetComponent,children:[
      {path:'Import',component:ImportComponent},
      {path:'Export',component:ExportComponent},
      {path:'DocumentGallery',component:DocumentgalleryComponent},
      {path:'Audit',component:AuditComponent},
      {path:'ImageGallery',component:ImagegalleryComponent},
    ]
   },
   {
     path:'Reports',component:ReportComponent,children:[
       {path:'AutomatedReport',component:AutomatedreportComponent},
       {
         path:'CustomReport',component:CustomreportComponent,children:[
          {path:'NewReport',component:NewreportComponent},
         ]
       },
       {
         path:'AssetReport',component:AssetComponent,children:[
           {path:'ByAssetTag',component:ByassettagComponent},
           {path:'ByTagWithPicture',component:BytagwithpictureComponent},
           {path:'ByCategory',component:BycategoryComponent},
           {path:'BySiteLocation',component:BysitelocationComponent},
           {path:'ByDepartment',component:BydepartmentComponent},
           {path:'ByWarrantyInfo',component:BywarrantyinfoComponent},
           {path:'ByLinkedAsset',component:BylinkedassetComponent},
         ]
       },
       {
         path:'AuditReport',component:AuditreportComponent,children:[
          {path:'ByAuditAssetTag',component:ByauditassettagComponent},
          {path:'ByFunding',component:ByfundingComponent},
          {path:'ByAuditSiteLocation',component:ByauditsitelocationComponent},
          {path:'ByAuditDate',component:ByauditdateComponent},
          {path:'LocationDiscrepancy',component:LocationdiscrepancyComponent},
          {path:'NonAuditAsset',component:NonauditassetComponent},
          {path:'NonAuditFunding',component:NonauditfundingComponent},
         ]
       }
     ]
   },
   {
     path:'Advance',component:AdvanceComponent,children:[
       {path:'ContractLicense',component:ContractlicenseComponent},
       {path:'AddContract',component:AddcontractComponent},
       {path:'PersonEmployee',component:PersonemployeeComponent},
       {path:'Customer',component:CustomerComponent},
       {path:'Insurance',component:InsuranceComponent},
       {path:'AddInsurance',component:AddinsuranceComponent},
     ]
   }
  ]
},
{ path: 'login', component: LoginComponent, pathMatch: 'full' },
{ path: 'register', component: RegisterComponent, pathMatch: 'full' },
{ path: 'forgetPassword', component: ForgrtpasswordComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
