import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationdiscrepancyComponent } from './locationdiscrepancy.component';

describe('LocationdiscrepancyComponent', () => {
  let component: LocationdiscrepancyComponent;
  let fixture: ComponentFixture<LocationdiscrepancyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocationdiscrepancyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationdiscrepancyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
