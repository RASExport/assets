import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ByauditsitelocationComponent } from './byauditsitelocation.component';

describe('ByauditsitelocationComponent', () => {
  let component: ByauditsitelocationComponent;
  let fixture: ComponentFixture<ByauditsitelocationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ByauditsitelocationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ByauditsitelocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
