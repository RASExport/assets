import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ByauditdateComponent } from './byauditdate.component';

describe('ByauditdateComponent', () => {
  let component: ByauditdateComponent;
  let fixture: ComponentFixture<ByauditdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ByauditdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ByauditdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
