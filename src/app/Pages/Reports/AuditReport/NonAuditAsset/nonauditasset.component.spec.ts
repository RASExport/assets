import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NonauditassetComponent } from './nonauditasset.component';

describe('NonauditassetComponent', () => {
  let component: NonauditassetComponent;
  let fixture: ComponentFixture<NonauditassetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NonauditassetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NonauditassetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
