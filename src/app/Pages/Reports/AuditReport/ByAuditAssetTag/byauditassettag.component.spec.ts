import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ByauditassettagComponent } from './byauditassettag.component';

describe('ByauditassettagComponent', () => {
  let component: ByauditassettagComponent;
  let fixture: ComponentFixture<ByauditassettagComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ByauditassettagComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ByauditassettagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
