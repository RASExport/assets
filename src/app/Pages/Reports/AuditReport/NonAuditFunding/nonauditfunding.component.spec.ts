import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NonauditfundingComponent } from './nonauditfunding.component';

describe('NonauditfundingComponent', () => {
  let component: NonauditfundingComponent;
  let fixture: ComponentFixture<NonauditfundingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NonauditfundingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NonauditfundingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
