import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ByfundingComponent } from './byfunding.component';

describe('ByfundingComponent', () => {
  let component: ByfundingComponent;
  let fixture: ComponentFixture<ByfundingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ByfundingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ByfundingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
