import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BysitelocationComponent } from './bysitelocation.component';

describe('BysitelocationComponent', () => {
  let component: BysitelocationComponent;
  let fixture: ComponentFixture<BysitelocationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BysitelocationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BysitelocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
