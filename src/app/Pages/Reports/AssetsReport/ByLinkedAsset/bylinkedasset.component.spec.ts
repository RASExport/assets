import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BylinkedassetComponent } from './bylinkedasset.component';

describe('BylinkedassetComponent', () => {
  let component: BylinkedassetComponent;
  let fixture: ComponentFixture<BylinkedassetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BylinkedassetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BylinkedassetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
