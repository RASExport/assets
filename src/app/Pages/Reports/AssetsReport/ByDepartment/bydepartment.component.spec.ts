import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BydepartmentComponent } from './bydepartment.component';

describe('BydepartmentComponent', () => {
  let component: BydepartmentComponent;
  let fixture: ComponentFixture<BydepartmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BydepartmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BydepartmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
