import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ByassettagComponent } from './byassettag.component';

describe('ByassettagComponent', () => {
  let component: ByassettagComponent;
  let fixture: ComponentFixture<ByassettagComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ByassettagComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ByassettagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
