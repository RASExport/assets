import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetreportComponent } from './assetreport.component';

describe('AssetreportComponent', () => {
  let component: AssetreportComponent;
  let fixture: ComponentFixture<AssetreportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssetreportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
