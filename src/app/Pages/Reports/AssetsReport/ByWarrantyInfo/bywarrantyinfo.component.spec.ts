import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BywarrantyinfoComponent } from './bywarrantyinfo.component';

describe('BywarrantyinfoComponent', () => {
  let component: BywarrantyinfoComponent;
  let fixture: ComponentFixture<BywarrantyinfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BywarrantyinfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BywarrantyinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
