import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BytagwithpictureComponent } from './bytagwithpicture.component';

describe('BytagwithpictureComponent', () => {
  let component: BytagwithpictureComponent;
  let fixture: ComponentFixture<BytagwithpictureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BytagwithpictureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BytagwithpictureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
