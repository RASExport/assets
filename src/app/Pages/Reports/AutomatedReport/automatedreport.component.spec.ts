import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomatedreportComponent } from './automatedreport.component';

describe('AutomatedreportComponent', () => {
  let component: AutomatedreportComponent;
  let fixture: ComponentFixture<AutomatedreportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutomatedreportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomatedreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
