import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractlicenseComponent } from './contractlicense.component';

describe('ContractlicenseComponent', () => {
  let component: ContractlicenseComponent;
  let fixture: ComponentFixture<ContractlicenseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContractlicenseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractlicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
