import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonemployeeComponent } from './personemployee.component';

describe('PersonemployeeComponent', () => {
  let component: PersonemployeeComponent;
  let fixture: ComponentFixture<PersonemployeeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonemployeeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonemployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
