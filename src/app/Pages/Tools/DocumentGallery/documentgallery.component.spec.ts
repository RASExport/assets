import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentgalleryComponent } from './documentgallery.component';

describe('DocumentgalleryComponent', () => {
  let component: DocumentgalleryComponent;
  let fixture: ComponentFixture<DocumentgalleryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocumentgalleryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentgalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
