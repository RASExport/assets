import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/Services/API/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  passMatch: boolean = false;
  returnUrl: string = "";
  validForm: boolean = true;
  registerForm: FormGroup;
  constructor(private route: ActivatedRoute,
    public API: ApiService,
    private router: Router) {
    this.registerForm = new FormGroup({
      Username: new FormControl('', [
        Validators.required]),
      Password: new FormControl('', [
        Validators.required,
        Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}')]),
      ConfirmPassword: new FormControl('', [Validators.required]),
      Email: new FormControl('', [Validators.required,
      Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
      MobileNo: new FormControl('', [
        Validators.required]),
      PhoneNo: new FormControl(''),
      DOB: new FormControl(''),
      CNIC: new FormControl(''),
      Gender: new FormControl(''),
      Address: new FormControl(''),
    })
  }
  ngOnInit(): void { }

  validations() {
    if (this.registerForm.controls.FirstName.value == "" || this.registerForm.controls.FirstName.value == null) {
      Swal.fire({
        text: 'Enter Username',
        icon: 'error',
        confirmButtonText: 'OK',
      });
      this.validForm = false;
      return;
    }
    if (this.registerForm.controls.Password.value == "" || this.registerForm.controls.Password.value == null) {
      Swal.fire({
        text: 'Enter Password',
        icon: 'error',
        confirmButtonText: 'OK',
      });
      this.validForm = false;
    }
    else {
      this.validForm = true;
    }
  }

  onRegisterSubmit() {
    this.registerForm.markAllAsTouched()
    if (this.registerForm.valid) {
      this.API.LoginUser('/api/Account/Register', this.registerForm.value).subscribe(
        (data) => {
          Swal.fire({
            text: data.message,
            icon: 'success',
            confirmButtonText: 'OK'
          });
          const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/login';
          this.router.navigate([returnUrl]);
        },

        (error) => {
          this.registerForm.enable({ emitEvent: true });
          if (error.error != undefined) {
            Swal.fire('POS', error.error.message, 'error');
          } else {
            Swal.fire('POS', 'Network Error.', 'error');
          }
        }
      );
    }
    else {
      return
    }
    // this.accountService
    //   .register(
    //     this.registerForm.controls.FirstName.value,
    //     this.registerForm.controls.Password.value
    //   )
    //   .pipe(first())
    //   .subscribe({
    //     next: () => {
    //       // get return url from query parameters or default to home page
    //       const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/login';
    //       Swal.fire({
    //         text: "User Created successfully.",
    //         icon: 'success',
    //         confirmButtonText: 'OK'
    //       });
    //       this.router.navigateByUrl(returnUrl);
    //     },
    //     error: error => {
    //       Swal.fire({
    //         text: "User Already exist.",
    //         icon: 'error',
    //         confirmButtonText: 'OK'
    //       });

    //     }
    //   });
  }
  passVerify() {
    if (this.registerForm.controls.Password.value === this.registerForm.controls.ConfirmPassword.value) {
      this.passMatch = true;
    }
    else {
      this.passMatch = false;
    }
  }
}
