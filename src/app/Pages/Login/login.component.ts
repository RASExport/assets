import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginViewModel } from './LoginModel';
import Swal from 'sweetalert2';
import { ApiService } from '../../Services/API/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  returnUrl: string="";
  LoginViewModel: LoginViewModel;
  validForm: boolean = true;
  loginForm: FormGroup;
  constructor(
    public API: ApiService, private route:ActivatedRoute,private router:Router) {
    this.LoginViewModel = new LoginViewModel();
    this.loginForm = new FormGroup({
      Username: new FormControl(''),
      Password: new FormControl(''),
    });
  }
  ngOnInit(): void {}
  validations() {
    if (this.loginForm.controls.Username.value == '' ) {
      Swal.fire({
        text: 'Enter Username.',
        icon: 'error',
        confirmButtonText: 'OK',
      });
      this.validForm = false;
      return;
    }
    if ( this.loginForm.controls.Password.value == '') {
      Swal.fire({
        text: 'Enter Password.',
        icon: 'error',
        confirmButtonText: 'OK',
      });
      this.validForm = false;
    }
  }
  onLoginSubmit() {
    this.validations();
    if (this.validForm) {
      this.API.LoginUser('/api/Account/Login',this.loginForm.value).subscribe(
        (data) => {
          localStorage.setItem('token', data.Access_Token);
          localStorage.setItem('userRoles', data.Roles);
          localStorage.setItem('userName', data.Refresh_Token);
          this.router.navigate([this.returnUrl]);
        },
        (error) => {
          this.loginForm.enable({ emitEvent: true });
          if (error.error != undefined) {
            Swal.fire('POS', error.error.message, 'error');
          } else {
            Swal.fire('POS', 'Network Error.', 'error');
          }
        }
      );
    }
  }
}
