import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeasereturnComponent } from './leasereturn.component';

describe('LeasereturnComponent', () => {
  let component: LeasereturnComponent;
  let fixture: ComponentFixture<LeasereturnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeasereturnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeasereturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
