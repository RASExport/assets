import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DataTablesModule } from 'angular-datatables';
import { LoginComponent } from './Pages/Login/login.component';
import { LayoutComponent } from './Pages/Shared/Layout/layout.component';
import { LeftmenuComponent } from './Pages/Shared/LeftMenu/leftmenu.component';
import { ToparComponent } from './Pages/Shared/TopBar/topar.component';
import { FooterComponent } from './Pages/Shared/Footer/footer.component';
import { DashboardComponent } from './Pages/DashBoard/dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TestComponent } from './Pages/Test/test.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RegisterComponent } from './Pages/Register/register.component';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { AddassetComponent } from './Pages/Assets/AddAssets/addasset.component';
import { AssetComponent } from './Pages/Assets/asset.component';
import { AssetlistComponent } from './Pages/Assets/AssetsList/assetlist.component';
import { CheckoutComponent } from './Pages/Assets/CheckOut/checkout.component';
import { CheckinComponent } from './Pages/Assets/CheckIn/checkin.component';
import { LeaseComponent } from './Pages/Assets/Lease/lease.component';
import { LeasereturnComponent } from './Pages/Assets/LeaseReturn/leasereturn.component';
import { DisposeComponent } from './Pages/Assets/Dispose/dispose.component';
import { MaintenanceComponent } from './Pages/Assets/Maintenance/maintenance.component';
import { MoveComponent } from './Pages/Assets/Move/move.component';
import { ReserveComponent } from './Pages/Assets/Reserve/reserve.component';
import { ToolComponent } from './Pages/Tools/tool.component';
import { ImportComponent } from './Pages/Tools/Import/import.component';
import { ExportComponent } from './Pages/Tools/Export/export.component';
import { DocumentgalleryComponent } from './Pages/Tools/DocumentGallery/documentgallery.component';
import { AuditComponent } from './Pages/Tools/Audit/audit.component';
import { ImagegalleryComponent } from './Pages/Tools/ImageGallery/imagegallery.component';
import { ReportComponent } from './Pages/Reports/report.component';
import { AutomatedreportComponent } from './Pages/Reports/AutomatedReport/automatedreport.component';
import { CustomreportComponent } from './Pages/Reports/CustomReport/customreport.component';
import { NewreportComponent } from './Pages/Reports/CustomReport/NewReport/newreport.component';
import { AssetreportComponent } from './Pages/Reports/AssetsReport/assetreport.component';
import { ByassettagComponent } from './Pages/Reports/AssetsReport/ByAssetTag/byassettag.component';
import { BytagwithpictureComponent } from './Pages/Reports/AssetsReport/ByTagWithPicture/bytagwithpicture.component';
import { BycategoryComponent } from './Pages/Reports/AssetsReport/ByCategory/bycategory.component';
import { BysitelocationComponent } from './Pages/Reports/AssetsReport/BySiteLocation/bysitelocation.component';
import { BydepartmentComponent } from './Pages/Reports/AssetsReport/ByDepartment/bydepartment.component';
import { BywarrantyinfoComponent } from './Pages/Reports/AssetsReport/ByWarrantyInfo/bywarrantyinfo.component';
import { BylinkedassetComponent } from './Pages/Reports/AssetsReport/ByLinkedAsset/bylinkedasset.component';
import { ForgrtpasswordComponent } from './Pages/ForgetPassword/forgrtpassword.component';
import { AuditreportComponent } from './Pages/Reports/AuditReport/auditreport.component';
import { ByauditassettagComponent } from './Pages/Reports/AuditReport/ByAuditAssetTag/byauditassettag.component';
import { ByauditdateComponent } from './Pages/Reports/AuditReport/ByAuditDate/byauditdate.component';
import { ByfundingComponent } from './Pages/Reports/AuditReport/ByFunding/byfunding.component';
import { ByauditsitelocationComponent } from './Pages/Reports/AuditReport/ByAuditSiteLocation/byauditsitelocation.component';
import { LocationdiscrepancyComponent } from './Pages/Reports/AuditReport/LocationDiscrepancy/locationdiscrepancy.component';
import { NonauditassetComponent } from './Pages/Reports/AuditReport/NonAuditAsset/nonauditasset.component';
import { NonauditfundingComponent } from './Pages/Reports/AuditReport/NonAuditFunding/nonauditfunding.component';
import { AdvanceComponent } from './Pages/Advance/advance.component';
import { ContractlicenseComponent } from './Pages/Advance/ContractLicenses/contractlicense.component';
import { AddcontractComponent } from './Pages/Advance/ContractLicenses/AddContract/addcontract.component';
import { PersonemployeeComponent } from './Pages/Advance/PersonEmployee/personemployee.component';
import { CustomerComponent } from './Pages/Advance/Customers/customer.component';
import { InsuranceComponent } from './Pages/Advance/Insurance/insurance.component';
import { AddinsuranceComponent } from './Pages/Advance/Insurance/AddInsurance/addinsurance.component'

const maskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LayoutComponent,
    LeftmenuComponent,
    ToparComponent,
    FooterComponent,
    DashboardComponent,
    TestComponent,
    RegisterComponent,
    AddassetComponent,
    AssetComponent,
    AssetlistComponent,
    CheckoutComponent,
    CheckinComponent,
    LeaseComponent,
    LeasereturnComponent,
    DisposeComponent,
    MaintenanceComponent,
    MoveComponent,
    ReserveComponent,
    ToolComponent,
    ImportComponent,
    ExportComponent,
    DocumentgalleryComponent,
    AuditComponent,
    ImagegalleryComponent,
    ReportComponent,
    AutomatedreportComponent,
    CustomreportComponent,
    NewreportComponent,
    AssetreportComponent,
    ByassettagComponent,
    BytagwithpictureComponent,
    BycategoryComponent,
    BysitelocationComponent,
    BydepartmentComponent,
    BywarrantyinfoComponent,
    BylinkedassetComponent,
    ForgrtpasswordComponent,
    AuditreportComponent,
    ByauditassettagComponent,
    ByauditdateComponent,
    ByfundingComponent,
    ByauditsitelocationComponent,
    LocationdiscrepancyComponent,
    NonauditassetComponent,
    NonauditfundingComponent,
    AdvanceComponent,
    ContractlicenseComponent,
    AddcontractComponent,
    PersonemployeeComponent,
    CustomerComponent,
    InsuranceComponent,
    AddinsuranceComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DataTablesModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgxMaskModule.forRoot(maskConfig),
  ],
  providers: [HttpClientModule],
  bootstrap: [AppComponent]

})
export class AppModule { }
